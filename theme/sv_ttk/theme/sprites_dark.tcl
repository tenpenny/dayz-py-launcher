set ::spriteinfo [list \
  card 0 0 50 50 \
  notebook-border 50 0 40 40 \
  tab-selected 90 0 32 32 \
  tab-rest 122 0 32 32 \
  tab-hover 154 0 32 32 \
  switch-rest 186 0 20 40 \
  switch-pressed 90 32 40 20 \
  switch-off-rest 50 40 40 20 \
  switch-off-pressed 0 50 40 20 \
  switch-off-hover 130 32 40 20 \
  switch-off-focus 170 40 40 20 \
  switch-off-focus-hover 90 52 40 20 \
  switch-off-dis 130 52 40 20 \
  switch-hover 40 60 40 20 \
  switch-focus 0 70 40 20 \
  switch-focus-hover 170 60 40 20 \
  switch-dis 80 72 40 20 \
  slider-trough-vert 40 80 22 22 \
  slider-trough-hor 0 90 22 22 \
  slider-thumb-rest 120 72 22 22 \
  slider-thumb-pressed 142 72 22 22 \
  slider-thumb-hover 164 80 22 22 \
  slider-thumb-focus 186 80 22 22 \
  slider-thumb-focus-hover 62 92 22 22 \
  slider-thumb-dis 84 92 22 22 \
  heading-rest 22 102 22 22 \
  heading-pressed 0 112 22 22 \
  heading-hover 106 94 22 22 \
  textbox-rest 128 94 20 20 \
  textbox-hover 44 114 20 20 \
  textbox-focus 22 124 20 20 \
  textbox-error 0 134 20 20 \
  textbox-dis 64 114 20 20 \
  radio-unsel-focus-hover 84 114 20 20 \
  radio-unsel-rest 0 154 20 20 \
  radio-unsel-pressed 0 174 20 20 \
  radio-unsel-hover 0 194 20 20 \
  radio-unsel-focus 148 102 20 20 \
  radio-unsel-dis 128 114 20 20 \
  radio-rest 104 116 20 20 \
  radio-pressed 168 102 20 20 \
  radio-hover 188 102 20 20 \
  radio-focus 42 134 20 20 \
  radio-focus-hover 20 144 20 20 \
  radio-dis 62 134 20 20 \
  button-rest 82 134 20 20 \
  button-pressed 20 164 20 20 \
  button-hover 20 184 20 20 \
  button-focus 82 134 20 20 \
  button-focus-hover 40 174 20 20 \
  button-dis 40 194 20 20 \
  button-accent-rest 60 154 20 20 \
  button-accent-pressed 60 174 20 20 \
  button-accent-hover 60 194 20 20 \
  button-accent-focus 60 154 20 20 \
  button-accent-focus-hover 80 174 20 20 \
  button-accent-dis 80 194 20 20 \
  scrollbar-trough-vert 208 80 12 20 \
  scrollbar-trough-hor 20 204 20 12 \
  scrollbar-thumb-vert 208 100 12 20 \
  scrollbar-thumb-hor 208 120 12 20 \
  check-hover 148 122 15 15 \
  check-unsel-rest 124 134 15 15 \
  check-unsel-pressed 102 136 15 15 \
  check-unsel-hover 163 122 15 15 \
  check-unsel-focus 178 122 15 15 \
  check-unsel-focus-hover 193 122 15 15 \
  check-unsel-dis 100 154 15 15 \
  check-tri-rest 100 169 15 15 \
  check-tri-pressed 100 184 15 15 \
  check-tri-hover 100 199 15 15 \
  check-tri-focus 139 137 15 15 \
  check-tri-focus-hover 117 149 15 15 \
  check-tri-dis 154 137 15 15 \
  check-rest 169 137 15 15 \
  check-pressed 184 137 15 15 \
  check-focus 169 137 15 15 \
  check-focus-hover 115 164 15 15 \
  check-dis 115 179 15 15 \
  empty 210 0 10 10 \
  sep 210 10 10 10 \
  progressbar-trough-vert 206 20 5 20 \
  progressbar-trough-hor 0 214 20 5 \
  progressbar-bar-vert 214 140 5 20 \
  progressbar-bar-hor 40 214 20 5 \
  grip 210 40 10 10 \
  up 210 50 10 5 \
  right 40 50 5 10 \
  down 210 55 10 5 \
  scrollbar-up 60 214 8 6 \
  scrollbar-right 170 32 6 8 \
  scrollbar-left 176 32 6 8 \
  scrollbar-down 68 214 8 6 \
]
