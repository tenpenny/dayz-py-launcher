Write-Host "--------------------------------------------------------------------------"
Write-Host ""
Write-Host "Yes - Installs Python, the Requests and pywin32 modules & the Launcher"
Write-Host "No  - Installs the modules and the Launcher"
Write-Host ""
Write-Host "--------------------------------------------------------------------------"

# Prompt user to install Python
Add-Type -AssemblyName PresentationCore,PresentationFramework
$promptType = [System.Windows.MessageBoxButton]::YesNoCancel
$promptBody = "Python is required for DayZ Py Launcher to run. If you already have it, just choose 'No'. Would you like to install Python?"
$promptTitle = "Install Python?"

$result = [System.Windows.MessageBox]::Show($promptBody,$promptTitle,$promptType)

$url = "https://gitlab.com/tenpenny/dayz-py-launcher/-/raw/main/dayz_py_installer.ps1"

if ($result -eq "Yes") {
    # Check if the OS is 64-bit
    $is64Bit = [System.Environment]::Is64BitOperatingSystem

    # Define the Python version
    $pythonVersion = "3.13.2"
    
    # Construct the download URL based on the OS architecture
    if ($is64Bit) {
        $pythonInstallerUrl = "https://www.python.org/ftp/python/$pythonVersion/python-$pythonVersion-amd64.exe"
    } else {
        $pythonInstallerUrl = "https://www.python.org/ftp/python/$pythonVersion/python-$pythonVersion.exe"
    }

    # Define the filename of the Python installer
    $pythonInstallerFileName = Split-Path -Path $pythonInstallerUrl -Leaf

    # Get the current user's username
    $username = $env:USERNAME

    # Remove dots and trim the version to only keep the first two parts (e.g., "3.13.2" becomes "313")
    $pythonVersionTrimmed = ($pythonVersion -split '\.')[0..1] -join ''

    # Construct the path to the Python directories dynamically using the trimmed $pythonVersion
    $pythonPath = "C:\Users\$username\AppData\Local\Programs\Python\Python$pythonVersionTrimmed"
    $pythonScriptsPath = "$pythonPath\Scripts"

    # Download Python installer
    $tempFile = "$env:TEMP\$pythonInstallerFileName"
    Invoke-WebRequest -Uri $pythonInstallerUrl -OutFile $tempFile

    # Run Python installer
    Start-Process -FilePath $tempFile -ArgumentList "/passive", "PrependPath=1", "Include_pip=1", "Include_tcltk=1", "AssociateFiles=1" -Wait
    
    # Reload environment variables
    $env:PATH += ";$pythonPath;$pythonScriptsPath"

    # Install required modules
    pip3 install requests pywin32
    
    # Delete the temporary Python installer file
    Remove-Item -Path $tempFile -Force

    # Download and install the Launcher
    Start-Process powershell.exe -ArgumentList "-Command", "Invoke-WebRequest -Uri $url -UseBasicParsing | Invoke-Expression"
    
    Write-Host "Installer Completed."
}
elseif ($result -eq "No") {
    # Install required modules
    pip3 install requests pywin32
    
    # Download and install the Launcher
    Start-Process powershell.exe -ArgumentList "-Command", "Invoke-WebRequest -Uri $url -UseBasicParsing | Invoke-Expression"
    
    Write-Host "Installer Completed."
}
else {
    Write-Host "Install Canceled."
}